import math
import sys
#KWAME OWUSU BOAHENE 81592019

# create vocabulary 
def createSentence(T):
    ray = ["what","when","where","how","where","which","how","who","whose","whom","is","are","the","an","it","is"]
    Fo = open(T,"r")
    listObj = []
    line = Fo.readline()
    while(len(line) != 0 ):
        for i in ray:
            if(i in line):
                line = line.replace(i,"")  
        listObj.append(line)
        line = Fo.readline()
    Fo.close()
    return(listObj)
   
    
    
    
#number of documents in respective classes
def classDoc(T):
    Fo = open(T,"r")
    count1 = 0
    count0 = 0
    ndoc = []
    line = Fo.readline()
    while(len(line) != 0 ):
        info = line.split()
        if (info[len(info)-1] == "1"):
            count1 = count1 + 1
        else:
            count0 = count0 + 1
        line=Fo.readline()
    
    ndoc.append(count1)
    ndoc.append(count0)
    
    Fo.close()
    return(ndoc)


#creates two arrays containing bag of words from each file
def bigDoc(C):
    bigdoc = []
    Positive = []
    Negative = []
    ray = [",","!",".","?",">","<","-",")","(",":"]
    Fo = open(C,"r")
    listObj = []
    
    line = Fo.readline()
    while(len(line) != 0 ):
        for i in ray:
            if(i in line):
                line = line.replace(i,"")  
        listObj = line.lower().split()
        for word in listObj:
            if (listObj[len(listObj)-1] == "1"):
                if (word != "1" ):
                    Positive.append(word)
            else:
                if (word != "0"):
                    Negative.append(word)
        line = Fo.readline()
    
    
    bigdoc.append(Positive)
    bigdoc.append(Negative)
    Fo.close()
    
    return(bigdoc)



def trainNaiveBayes(D):

    
    CL = classDoc(D)
    bigdoc= bigDoc(D)
    
    loghood=[]
    logprior =[]
    loghoodP={}
    loghoodN={}
    
      #log of number of docments in repective class / number of documents
    for c in CL:
        logprior.append(math.log(c/(CL[0]+CL[1])))

 
    
    
    wdc = 0
    wdp = 0
    wdn = 0
    
    V = createVocab(D)
    
        
    dictWord = {}
    dictWordC = {}
    dictWordD = {}
    
    
    #count of word w in both class
    for w in V:
        for wd in bigdoc:
            for word in wd:
                if(wd == w):
                    wdc = wdc+1
        dictWord[w]=wdc
        wdc=0


    #count of word w in Positive class
    for w in V:
        for wd in bigdoc[0]:
            if(w == wd):
                wdp = wdp +1
        dictWordC[w] = wdp
        wdp = 0


    #count of word w in Negative class
    for w in V:
        for wd in bigdoc[1]:
            if(w == wd):
                wdn = wdn +1
        dictWordD[w] = wdn
        wdn = 0

            
    
                            
  
    #for clas in bigdoc:
    count = 0.0
    count2 = 0.0
    multi = 1.0
    

    dictWord.get(word)
    
    #calculating loglikelihood of Positive class
    val = 0
    for word in V:
        count = dictWordC[word]
        for key in dictWord:
            if (key != word):
                val = val + dictWord[key] 
                
        multi = math.log((count +1)/float((val + 1)))
        loghoodP[word] = multi
    
    
    #calculating loglikelihood of Negative class
    val = 0
    for word in V:
        count = dictWordD[word]
        for key in dictWord:
            if (key != word):
                val = val + dictWord[key] 
                
        multi = math.log((count +1)/float((val + 1)))
        loghoodN[word] = multi
        
        
   
        


    return(logprior,loghoodP,loghoodN,V)

          
#createVocab("imdb_labelled.txt")
#bigDoc("trainer.txt")
#classMaker("trainer.txt","class.txt") 
#classDoc("imdb_labelled.txt")




def testNaiveBayes(testdoc):
    train = trainNaiveBayes("trainer.txt")
    
    Fo = open(testdoc,"r")
    To = open("results.txt","w")
    
    sumx = []
    sumx = train[0]
    loghoodP = train[1]
    loghoodN = train[2]
    V = train[3]
    
    
  #  return(logprior,loghoodP,loghoodN,V)
    
    
    ray = [",","!",".","?",">","<","-",")","(",":"]
    
    line = Fo.readline()
    while(len(line) != 0 ):
        for i in ray:
            if(i in line):
                line = line.replace(i,"")  
        listObj = line.lower().split()
        for word in listObj:
            for w in V:
                if(word==w):
                    
                    sumx[0] = sumx[0] + loghoodP[word]
                    sumx[1] = sumx[1] + loghoodN[word]
        if(sumx[0]>sumx[1]):
            To.write("1\n")
        else:
            To.write("0\n")
        line = Fo.readline()
        sumx = train[0]
        
    Fo.close()
    To.close()
    
    

#trainNaiveBayes("trainer.txt")

            

if __name__ == "__main__":
    testNaiveBayes(sys.argv[1])
else:
    print("Try again and add file name")
