# Final Project for NLP
This is a repository to for an NLP Final Project on Topic Modelling and Question and Answering. Three Machine Learning/NLP Models would be applied on each of the two Tasks on a give dataset of FAQs from a wide-range of topics.

## Topic Modelling
For Topic Modelling, Naive Bayes Classification, Logistics Regression Classification and Neural Networks for Classification would be implemented to solve the task given the dataset.

## Question and Answering
For Question and Answering, Cosine Similarity and QR Pairs, Deep Learning and Information Retreival Approach


## Team Members
Emmanuel Jojoe Ainoo

Kwame Boahene

David Sasu

Joseph Brown-Pobee

Benjamin Kusi
