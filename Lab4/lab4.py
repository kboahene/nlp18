
#Kwame Owusu Boahene 81592019
import sys
import nltk
from nltk import word_tokenize, sent_tokenize

#imoport scikit learn library
import sklearn
#Import  CountVectorizer
from sklearn.feature_extraction.text import CountVectorizer
#Import 
from sklearn.feature_extraction.text import TfidfTransformer
# Import Multinominal Naive Bayes Model
from sklearn.naive_bayes import MultinomialNB

#Import train_test_split to split dataset into training and testing sets
from sklearn.model_selection import train_test_split

#import logisticRegression Modal
from sklearn.linear_model import LogisticRegression



# create sentences
def createSentences(T):
    Sentences = []
    Fo = open(T,"r")
    line = Fo.readline()
    while(len(line) != 0 ):
        # take out the classification for each sentence
        line = line.replace("\t1\n","")
         # take out the classification for each sentence
        line = line.replace("\t0\n","")
        Sentences.append(line)
        line = Fo.readline()
    Fo.close()
    
    #print(Sentences)
    return(Sentences)


#extract sentences
def extractSentences(T):
    Sentences = []
    Fo = open(T,"r")
    line = Fo.readline()
    while(len(line) != 0 ):
        Sentences.append(line)
        line = Fo.readline()
    Fo.close()
    
    #print(Sentences)
    return(Sentences)
    
   

#createSentences("amazon_cells_labelled.txt")

# extract corresponding class from trainer file
def clas(T):
    Class = []
    Vals = [];
    Fo = open(T,"r")
    line = Fo.readline()
    while(len(line) != 0 ):
        Vals = line.split()
        if (Vals[len(Vals)-1] == "1"):
            t = 1
            Class.append(t);
        else:
            t= 0
            Class.append(t)
        line=Fo.readline()
    Fo.close()
    
    return(Class)
   

#clas("amazon_cells_labelled.txt")

#logistic and Naive Bayes without normalization
def naiveNormalized(traindoc,test):
    # normalize data
    review_vec = CountVectorizer(min_df=1, tokenizer=nltk.word_tokenize)
    Sentence = createSentences(traindoc)
    testdoc = extractSentences(test)
    Classes = clas(traindoc)
    reviewcounter = review_vec.fit_transform(Sentence)

    #Converting raw frequency counts into TF-IDF values
    tfidf_transformer = TfidfTransformer()
    review_tfidf = tfidf_transformer.fit_transform(reviewcounter)

    # Split data into training and test sets
    docs_train, docs_test, y_train, y_test = train_test_split(review_tfidf, Classes, test_size = 0.20, random_state = 12)

    # Train Naive Bayes classifier
    trainer = MultinomialNB().fit(docs_train, y_train)

    # Predicting the Test set results
    y_pred = trainer.predict(docs_test)

    #calculate accuracy
    #print('Accuracy on the test subset for Naive Bayes is: {:.3f}'.format(sklearn.metrics.accuracy_score(y_test, y_pred)))
    reviewdoc = review_vec.transform(testdoc)
    new_tfidf = tfidf_transformer.transform(reviewdoc)

    pred = trainer.predict(new_tfidf)
    
    #print(pred)
    To = open("results-nb-n.txt","w")
    for i in pred:
        To.write(str(i)+" \n")
    To.close()
    


def logNormalized(traindoc,test):
    #Normalize data
    review_vec = CountVectorizer(min_df=1, tokenizer=nltk.word_tokenize)
    Sentence = createSentences(traindoc)
    testdoc = extractSentences(test)
    Classes = clas(traindoc)
    reviewcounter = review_vec.fit_transform(Sentence)

    #Converting raw frequency counts into TF-IDF values
    tfidf_transformer = TfidfTransformer()
    review_tfidf = tfidf_transformer.fit_transform(reviewcounter)

    # Split data into training and test sets
    docs_train, docs_test, y_train, y_test = train_test_split(review_tfidf, Classes, test_size = 0.20, random_state = 12)


    
    #Intialize Logistic Regression
    log_reg = LogisticRegression(solver='liblinear')

    # Train Logistic Regression classifier
    clf = log_reg.fit(docs_train, y_train)
    

 #   print('Accuracy on the training subset for Logistic Rgression: {:.3f}'.format(log_reg.score(docs_train, y_train)))
   # print('Accuracy on the test subset for Logistic Rgression: {:.3f}'.format(log_reg.score(docs_test, y_test)))
    
    reviewdoc = review_vec.transform(testdoc)
    new_tfidf = tfidf_transformer.transform(reviewdoc)

    pred = clf.predict(new_tfidf)
    
    #print(pred)
    To = open("results-lr-n.txt","w")
    for i in pred:
        To.write(str(i)+" \n")
    To.close()

    
#logistic and Naive Bayes with normalization
def naiveUnormalized(traindoc,test):
    review_vec = CountVectorizer()
    Sentence = createSentences(traindoc)
    testdoc = extractSentences(test)
    Classes = clas(traindoc)
    reviewcounter = review_vec.fit_transform(Sentence)

    #Converting raw frequency counts into TF-IDF values
    tfidf_transformer = TfidfTransformer()
    review_tfidf = tfidf_transformer.fit_transform(reviewcounter)

    # Split data into training and test sets
    docs_train, docs_test, y_train, y_test = train_test_split(review_tfidf, Classes, test_size = 0.20, random_state = 12)

    # Train Naive Bayes classifier
    trainer = MultinomialNB().fit(docs_train, y_train)

    # Predicting the Test set results
    y_pred = trainer.predict(docs_test)

    #calculate accuracy
   # print('Accuracy on the test subset for Naive Bayes is: {:.3f}'.format(sklearn.metrics.accuracy_score(y_test, y_pred)))
    
    reviewdoc = review_vec.transform(testdoc)
    new_tfidf = tfidf_transformer.transform(reviewdoc)

    pred = trainer.predict(new_tfidf)
    
    #print(pred)
    To = open("results-nb-u.txt","w")
    for i in pred:
        To.write(str(i)+" \n")
    To.close()
    
    

def logUnormalized(traindoc,test):
    review_vec = CountVectorizer()
    Sentence = createSentences(traindoc)
    testdoc = extractSentences(test)
    Classes = clas(traindoc)
    reviewcounter = review_vec.fit_transform(Sentence)

    #Converting raw frequency counts into TF-IDF values
    tfidf_transformer = TfidfTransformer()
    review_tfidf = tfidf_transformer.fit_transform(reviewcounter)

    # Split data into training and test sets
    docs_train, docs_test, y_train, y_test = train_test_split(review_tfidf, Classes, test_size = 0.20, random_state = 12)


    #Intialize Logistic Regression
    log_reg = LogisticRegression(solver='liblinear')

    # Train Logistic Regression classifier
    clf = log_reg.fit(docs_train, y_train)
    

 #   print('Accuracy on the training subset for Logistic Rgression: {:.3f}'.format(log_reg.score(docs_train, y_train)))
   # print('Accuracy on the test subset for Logistic Rgression: {:.3f}'.format(log_reg.score(docs_test, y_test)))
    
    reviewdoc = review_vec.transform(testdoc)
    new_tfidf = tfidf_transformer.transform(reviewdoc)

    pred = clf.predict(new_tfidf)
    
    #print(pred)
    
    To = open("results-lr-u.txt","w")
    for i in pred:
        To.write(str(i)+" \n")
    To.close()



if __name__ == "__main__":
    
    if sys.argv[1] == "nb" and sys.argv[2] == "u":
        naiveUnormalized("trainer.txt",sys.argv[3])
    elif sys.argv[1] == "nb" and sys.argv[2] == "n":
        naiveNormalized("trainer.txt",sys.argv[3])
    elif sys.argv[1] == "lr" and sys.argv[2] == "u":
        logUnormalized("trainer.txt",sys.argv[3])
    elif sys.argv[1] == "lr" and sys.argv[2] == "n":
        logNormalized("trainer.txt",sys.argv[3])
    
    
else:
    print("Try again and add file name")

