

from numpy import * 
import math
import sys

def minEditDistance(source, target):
    n = len(source)+1
    m = len(target)+1
    
    md = zeros((n,m), dtype=int)
    
    #rows
    for i in range(1,n):
        md[i][0] = i
    #columns
    for j in range(1,m):
        md[0][j] = j
    
    for i in range(1,n):
        for j in range(1,m):
            #if characters at i-1 and j-1 are the same cost is 0
            if(source[i-1]==target[j-1]): 
                cost = 0
            #the cost is 0
            else:
                cost = 2
            md[i][j] = min(md[i-1][j]+1,md[i-1][j-1]+ cost,md[i][j-1]+1)
            
    print("The minimum edit distance is ",md[n-1][m-1])    
    return md[n-1][m-1]

#minEditDistance("intention","execution")

if __name__ == "__main__":
    minEditDistance(sys.argv[1],sys.argv[2])
else:
    print("Please enter two arguments")
    
    
    

